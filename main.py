import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np


def run():
    x = 1125
    x2 = 7125
    x3 = 11625
    data = [x, x + 300, x + 600, x2, x2 + 300, x2 + 600, x3, x3 + 300, x3 + 600]
    data_interval = [0, 4000, 8000, 12000, 16000]
    width = 250
    bottom = 0

    fig = plt.figure(figsize=(16, 4))
    ax = fig.add_subplot(1, 1, 1)

    plt.bar(data, 1, width=width, bottom=bottom, color='cyan')

    plt.bar(data_interval, 2, width=20, bottom=bottom, color='black')

    arrow_level = 0.4
    last_arrow_level = 0.3

    arrow1 = patches.FancyArrowPatch((1000, arrow_level), (4000, arrow_level), arrowstyle='<->', mutation_scale=15)
    arrow2 = patches.FancyArrowPatch((7000, arrow_level), (8000, arrow_level), arrowstyle='<->', mutation_scale=15)
    arrow3 = patches.FancyArrowPatch((11500, arrow_level), (12000, arrow_level), arrowstyle='<->', mutation_scale=15)
    arrow4 = patches.FancyArrowPatch((11500, last_arrow_level), (16000, last_arrow_level), arrowstyle='<->', mutation_scale=15)

    ax.add_patch(arrow1)
    ax.add_patch(arrow2)
    ax.add_patch(arrow3)
    ax.add_patch(arrow4)

    ax.text(2450, arrow_level+0.02, '3s')
    ax.text(7450, arrow_level+0.02, '1s')
    ax.text(11670, arrow_level+0.02, '1s')
    ax.text(13700, last_arrow_level+0.02, '5s')

    plt.tight_layout()
    plt.xlabel("Time")

    ax.axes.yaxis.set_visible(False)
    plt.xlim([0, 17000])
    # Major ticks every 4000, minor ticks every 1000
    major_ticks = np.arange(0, 17000, 4000)
    minor_ticks = np.arange(0, 17000, 1000)

    ax.set_xticks(major_ticks)
    ax.set_xticks(minor_ticks, minor=True)

    # And a corresponding grid
    ax.grid(which='both')

    # Or if you want different settings for the grids:
    ax.grid(which='minor', alpha=0.2)
    ax.grid(which='major', alpha=0.5)

    plt.show()
    # plt.savefig('paradigm.png')


if __name__ == '__main__':
    run()
